# ffmpeg-wrapper

A shell script to wrap ffmpeg, fixing some odd issue with using cuda hwaccel arguments not being the last arguments in the invocation. This is especially useful for Jellyfin/Emby/Plex since they don't really let you customize the ffmpeg args.

To use, just make sure NVENC hardware acceleration is enabled, and point your plex/emby/jellyfin FFmpeg path to ffmpeg.py (and you might also want to double check that ffmpeg is using the correct ffmpeg binary).

Some of the symptoms that you might need this are errors such as the following in your logs

```
ffmpeg version n4.4 Copyright (c) 2000-2021 the FFmpeg developers
  built with gcc 11.1.0 (GCC)
  configuration: --prefix=/usr --disable-debug --disable-static --disable-stripping --enable-amf --enable-avisynth --enable-cuda-llvm --enable-lto --enable-fontconfig --enable-gmp --enable-gnutls --enable-gpl --enable-ladspa --enable-libaom --enable-libass --enable-libbluray --enable-libdav1d --enable-libdrm --enable-libfreetype --enable-libfribidi --enable-libgsm --enable-libiec61883 --enable-libjack --enable-libmfx --enable-libmodplug --enable-libmp3lame --enable-libopencore_amrnb --enable-libopencore_amrwb --enable-libopenjpeg --enable-libopus --enable-libpulse --enable-librav1e --enable-librsvg --enable-libsoxr --enable-libspeex --enable-libsrt --enable-libssh --enable-libsvtav1 --enable-libtheora --enable-libv4l2 --enable-libvidstab --enable-libvmaf --enable-libvorbis --enable-libvpx --enable-libwebp --enable-libx264 --enable-libx265 --enable-libxcb --enable-libxml2 --enable-libxvid --enable-libzimg --enable-nvdec --enable-nvenc --enable-shared --enable-version3
  libavutil      56. 70.100 / 56. 70.100
  libavcodec     58.134.100 / 58.134.100
  libavformat    58. 76.100 / 58. 76.100
  libavdevice    58. 13.100 / 58. 13.100
  libavfilter     7.110.100 /  7.110.100
  libswscale      5.  9.100 /  5.  9.100
  libswresample   3.  9.100 /  3.  9.100
  libpostproc    55.  9.100 / 55.  9.100
Input #0, avi, from 'file:<redacted>':
  Metadata:
    software        : VirtualDubMod 1.5.4.1 (build 2178/release)
    IAS1            : English
  Duration: 00:22:09.00, start: 0.000000, bitrate: 1099 kb/s
  Stream #0:0: Video: mpeg4 (Advanced Simple Profile) (XVID / 0x44495658), yuv420p, 640x480 [SAR 1:1 DAR 4:3], 964 kb/s, 23.98 fps, 23.98 tbr, 23.98 tbn, 23.98 tbc
  Stream #0:1: Audio: mp3 (U[0][0][0] / 0x0055), 48000 Hz, stereo, fltp, 128 kb/s
Stream mapping:
  Stream #0:0 -> #0:0 (mpeg4 (native) -> h264 (h264_nvenc))
  Stream #0:1 -> #0:1 (copy)
Press [q] to stop, [?] for help
[h264_nvenc @ 0x564279291700] The selected preset is deprecated. Use p1 to p7 + -tune or fast/medium/slow.
Output #0, hls, to '<redacted>':
  Metadata:
    encoder         : Lavf58.76.100
  Stream #0:0: Video: h264 (High), cuda(progressive), 640x480 [SAR 1:1 DAR 4:3], q=2-31, 4824 kb/s, 23.98 fps, 90k tbn
    Metadata:
      encoder         : Lavc58.134.100 h264_nvenc
    Side data:
      cpb: bitrate max/min/avg: 4824630/0/4824630 buffer size: 9649260 vbv_delay: N/A
  Stream #0:1: Audio: mp3 (U[0][0][0] / 0x0055), 48000 Hz, stereo, fltp, 128 kb/s
frame=    1 fps=0.0 q=0.0 size=N/A time=00:00:00.00 bitrate=N/A speed=   0x    
[mpeg4 @ 0x5642797252c0] No decoder surfaces left
[mpeg4 @ 0x564279728900] No decoder surfaces left
[mpeg4 @ 0x56427972bf40] No decoder surfaces left
[mpeg4 @ 0x564279703440] No decoder surfaces left
[mpeg4 @ 0x564279706a80] No decoder surfaces left
Error while decoding stream #0:0: Cannot allocate memory
[mpeg4 @ 0x5642797252c0] No decoder surfaces left
Error while decoding stream #0:0: Cannot allocate memory
    Last message repeated 2 times
[mpeg4 @ 0x56427972bf40] No decoder surfaces left
[mpeg4 @ 0x564279703440] No decoder surfaces left
Error while decoding stream #0:0: Cannot allocate memory
[mpeg4 @ 0x564279706a80] No decoder surfaces left
Impossible to convert between the formats supported by the filter 'Parsed_null_0' and the filter 'auto_scaler_0'
Error reinitializing filters!
Failed to inject frame into filter network: Function not implemented
Error while processing the decoded data for stream #0:0
Conversion failed!
```
