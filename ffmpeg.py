#!/usr/bin/env python3
import sys
import os
print(sys.argv)
args=[]
for i in sys.argv[1:]:
    if i not in ['cuda', '-hwaccel', '-hwaccel_output_format']:
        args.append("\""+ i+ "\"")
args = ['/usr/bin/ffmpeg'] + args + ['-hwaccel', 'cuda']
print(args)
os.system(str.join(' ', args))
